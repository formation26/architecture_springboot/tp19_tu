package fr.epita.tp19.application;

public interface CalculService {
	
	int add(int a, int b);
	
	int sub(int a, int b);

}
