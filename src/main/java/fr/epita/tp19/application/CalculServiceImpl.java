package fr.epita.tp19.application;

public class CalculServiceImpl implements CalculService {

	public int add(int a, int b) {

		return a+b;
	}

	@Override
	public int sub(int a, int b) {
		
		if(a<0 || b<0) {
			throw new IllegalArgumentException();
		}
		return a-b;
	}

}
