package fr.epita.tp19.application;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CalculServiceTest {
	
	 CalculService calcul;
	
	@BeforeEach
	public  void setUp() {
	
		calcul=new CalculServiceImpl();
	}
	
	@Test
	public void add() {
       assertEquals(5,calcul.add(2, 3));
	}
	
	@Test
	public void sub() {
		
		assertEquals(-1,calcul.sub(2, 3));
		//assertEquals(1,calcul.sub(-2, 3)); //Déclenche l'exception test rouge
		assertThrows(IllegalArgumentException.class, ()->calcul.sub(-2, 3));
	}

}
